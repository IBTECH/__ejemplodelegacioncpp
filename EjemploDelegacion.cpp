#include <vector>
#include <string>

using namespace std;

class Proyecto
{
public:
};

class Trabajador
{
public:
	virtual void retroalimentar(string retroalimentacion) = 0;
};

class GerenteOperaciones : public Trabajador
{
public:
	virtual bool dimeEstatusEmpresa() = 0;
	virtual vector<Trabajador *> reportaPersonalIncorrecto() = 0;
	virtual void gestionaProyectos(vector<Proyecto*> proyectos) = 0;
};

class GerenteMarketing : public Trabajador
{
public:
	virtual int cantidadDePersonasLlegadasConAnuncios() = 0;
	virtual void buscarNuevaEstrategiaMarketing() = 0;
};

class GerenteVentas : public Trabajador
{
public:
	virtual vector<Proyecto *> proyectosCerrados() = 0;
};

class Gamer
{
public:
	virtual void teRetoEnHalo() = 0;
};




// Controlador
class DirectorNodus : public Trabajador
{
private:
	int cantidadMinimaPersonal;
	vector<Trabajador *> trabajadores;

public:

	DirectorNodus(GerenteOperaciones * gerenteOperaciones = 0, GerenteMarketing * gerenteMarketing = 0, GerenteVentas * gerenteVentas = 0) 
		: cantidadMinimaPersonal(10), gerenteOperaciones(gerenteOperaciones), gerenteMarketing(gerenteMarketing), gerenteVentas(gerenteVentas)
	{

	}

	void generarProductos()
	{
		if(gerenteOperaciones) {
			bool estatus = gerenteOperaciones->dimeEstatusEmpresa();
			if(!estatus) {
				vector<Trabajador *> trabajoresMalPortados = gerenteOperaciones->reportaPersonalIncorrecto();
				for(vector<Trabajador *>::iterator it_trabajadores = trabajoresMalPortados.begin(); it_trabajadores != trabajoresMalPortados.end(); ++it_trabajadores) {
					Trabajador * trabajadorInteres = *it_trabajadores;

					trabajadorInteres->retroalimentar("Trabaja mas!!!");
				}
			}

			if(gerenteVentas) {
				vector<Proyecto*> proyectosCerrados = gerenteVentas->proyectosCerrados();

				gerenteOperaciones->gestionaProyectos(proyectosCerrados);
			}
		}

		if(gerenteMarketing) {
			int cantidadPersonalInteresado = gerenteMarketing->cantidadDePersonasLlegadasConAnuncios();
			if (cantidadPersonalInteresado < cantidadMinimaPersonal) {
				gerenteMarketing->buscarNuevaEstrategiaMarketing();
			}
		}
	}

	void disfrutarTiempoLibre()
	{
		for(vector<Trabajador *>::iterator it_trabajadores = trabajadores.begin(); it_trabajadores != trabajadores.end(); ++it_trabajadores)  {
			Trabajador * t = *it_trabajadores;
			if(Gamer * gamer = dynamic_cast<Gamer *>(t)) {
				gamer->teRetoEnHalo();
			}
		}
	}

	void retroalimentar(string retroalimentacion)
	{
		// TODO En funcion de la retroalimentacion obtenida, retroalimentar al equipo de trabajo
	}

	GerenteOperaciones * gerenteOperaciones;
	GerenteMarketing * gerenteMarketing;
	GerenteVentas * gerenteVentas;

};



int main()
{
	DirectorNodus director;
	director.generarProductos();
	return 0;
}

